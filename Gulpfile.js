var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var expect = require('gulp-expect-file');
var del = require('del');
var sass = require('gulp-sass');
var merge = require('merge-stream');

var paths = {

	scripts: [
		//   ADMIN LTE Core JS Files
		'vendor/almasaeed2010/adminlte/plugins/jquery/jquery.min.js',
		'vendor/almasaeed2010/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js',
		'vendor/almasaeed2010/adminlte/dist/js/adminlte.min.js',
		//select2
		'vendor/almasaeed2010/adminlte/plugins/select2/js/select2.full.min.js',
		'vendor/almasaeed2010/adminlte/plugins/select2/js/i18n/cs.js',

		//summernote
		'vendor/almasaeed2010/adminlte/plugins/summernote/summernote-bs4.js',

		//sweet alert pro lepsi bootstrap hlasky
		'vendor/almasaeed2010/adminlte/plugins/sweetalert2/sweetalert2.js',

		//nette ajax
		'node_modules/nette.ajax.js/nette.ajax.js',

		//ublaboo datagrid
		'vendor/ublaboo/datagrid/assets/datagrid.js',
		'vendor/ublaboo/datagrid/assets/datagrid-instant-url-refresh.js',
		'vendor/ublaboo/datagrid/assets/datagrid-spinners.js',


		'vendor/nette/forms/src/assets/netteForms.js',
		'www/js/main.js',
	],
	css: [
		'vendor/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css',
		'vendor/almasaeed2010/adminlte/plugins/select2/css/select2.min.css',
		'vendor/almasaeed2010/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css',
		'vendor/almasaeed2010/adminlte/plugins/sweetalert2/sweetalert2.css',
		'vendor/ublaboo/datagrid/assets/datagrid.css',
		'vendor/ublaboo/datagrid/assets/datagrid-spinners.css',
		'vendor/almasaeed2010/adminlte/plugins/summernote/summernote-bs4.css',
	],
	scss: [
		'www/css/style.scss',

	]
};


gulp.task('copy', function () {
	gulp.src([
		'vendor/almasaeed2010/adminlte/plugins/fontawesome-free/webfonts/*'])
		.pipe(gulp.dest('www/webfonts/'));

	gulp.src([
		'vendor/almasaeed2010/adminlte/plugins/summernote/font/*'])
		.pipe(gulp.dest('www/css/font/'));

});
// Not all tasks need to use streams
// A gulpfile is just another node program and you can use any package available on npm
gulp.task('clean', function () {
	// You can use multiple globbing patterns as you would with `gulp.src`
	return del(['build']);
});

gulp.task('scripts', ['clean'], function () {
	// Minify and copy all JavaScript (except vendor scripts)
	// with sourcemaps all the way down
	return gulp.src(paths.scripts)
		//  .pipe(uglify())
		.pipe(expect(paths.scripts))
		.pipe(concat('all.min.js'))
		.pipe(gulp.dest('www/js'));
});

// Copy all static images
gulp.task('css', ['clean'], function () {
	var scss = gulp.src(paths.scss)
		.pipe(expect(paths.scss))
		.pipe(sass());

	var css = gulp.src(paths.css).pipe(expect(paths.css));

	var mergedStream = merge(css, scss)
		.pipe(concat('all.min.css'))
		// .pipe(minify())
		.pipe(gulp.dest('www/css'));

	return mergedStream;
});

gulp.task("sass", function () {
	gulp.src(sassFiles)
		.pipe(sass({style: 'expanded'}))
		.pipe(autoprefixer("last 3 version", "safari 5", "ie 8", "ie 9"))
		.pipe(gulp.dest("target/css"))
		.pipe(rename({suffix: '.min'}))
		.pipe(minifycss())
		.pipe(gulp.dest('target/css'));
});

// Rerun the task when a file changes
gulp.task('watch', function () {
	gulp.watch(paths.scripts, ['scripts']);
	gulp.watch(paths.css, ['css']);
	gulp.watch(paths.scss, ['css']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'scripts', 'css', 'copy']);
gulp.task('deploy', ['scripts', 'css', 'copy']);