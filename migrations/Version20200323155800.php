<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200323155800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE transport (id INT AUTO_INCREMENT NOT NULL, created_user_id INT NOT NULL, driver_id INT DEFAULT NULL, from_address_id INT NOT NULL, to_address_id INT DEFAULT NULL, description LONGTEXT NOT NULL, status VARCHAR(255) NOT NULL COMMENT \'(DC2Type:string_enum)\', INDEX IDX_66AB212EE104C1D3 (created_user_id), INDEX IDX_66AB212EC3423909 (driver_id), INDEX IDX_66AB212EDE136972 (from_address_id), INDEX IDX_66AB212ED2844D08 (to_address_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE transport ADD CONSTRAINT FK_66AB212EE104C1D3 FOREIGN KEY (created_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transport ADD CONSTRAINT FK_66AB212EC3423909 FOREIGN KEY (driver_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transport ADD CONSTRAINT FK_66AB212EDE136972 FOREIGN KEY (from_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE transport ADD CONSTRAINT FK_66AB212ED2844D08 FOREIGN KEY (to_address_id) REFERENCES address (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE transport');
    }
}
