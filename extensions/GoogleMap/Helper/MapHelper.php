<?php
declare(strict_types = 1);

namespace Extensions\GoogleMap\Helper;

use Ivory\GoogleMap\Base\Coordinate;
use Ivory\GoogleMap\Map;
use Ivory\GoogleMap\Overlay\Marker;
use Ivory\GoogleMap\Service\Geocoder\GeocoderService;
use Ivory\GoogleMap\Service\Geocoder\Request\GeocoderAddressRequest;
use Ivory\GoogleMap\Service\Geocoder\Response\GeocoderResult;

class MapHelper
{

	/** @var GeocoderService */
	protected $geocoderService;

	public function __construct(GeocoderService $geocoderService)
	{
		$this->geocoderService = $geocoderService;
	}

	public function addMarkerToMap(string $name, string $address, Map $map)
	{
		$response = $this->geocoderService->geocode(new GeocoderAddressRequest($address));
		/** @var GeocoderResult $result */
		$result = $response->getResults()[0];
		$coordinate = $result->getGeometry()->getLocation();

		$marker = new Marker($coordinate);
		$map->getOverlayManager()->addMarker($marker);

	}

	public function createMap(): Map
	{
		$map = new Map();

		$map->setAutoZoom(false);

		// Sets the center
		$map->setCenter(new Coordinate(49.8037633, 15.4749126));

		$map->setMapOption('zoom', 7);

		return $map;
	}
}