<?php
declare(strict_types = 1);

namespace Extensions\GoogleMap\DI;

use Extensions\GoogleMap\Helper\MapHelper;
use Http\Adapter\Guzzle6\Client;
use Http\Client\Common\Plugin\CachePlugin;
use Http\Client\Common\PluginClient;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use Http\Message\StreamFactory\GuzzleStreamFactory;
use Ivory\GoogleMap\Helper\ApiHelper;
use Ivory\GoogleMap\Helper\Builder\ApiHelperBuilder;
use Ivory\GoogleMap\Helper\Builder\MapHelperBuilder;
use Ivory\GoogleMap\Service\Geocoder\GeocoderService;
use Nette\DI\CompilerExtension;
use Nette\Utils\Strings;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class GoogleMapExtension extends CompilerExtension
{
	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();
		$config = (array) $this->getConfig();

		$client = $builder->addDefinition($this->prefix('client'))
						  ->setType(Client::class)
						  ->setAutowired(FALSE);
		$filesystemAdapter = $builder->addDefinition($this->prefix('filesystemAdapter'))
									 ->setType(FilesystemAdapter::class)
									 ->setArguments([Strings::webalize(__CLASS__), 0, $builder->parameters['tempDir']])
									 ->setAutowired(FALSE);
		$guzzleStreamFactory = $builder->addDefinition($this->prefix('guzzleStreamFactory'))
									   ->setType(GuzzleStreamFactory::class)
									   ->setAutowired(FALSE);

		$cachePlugin = $builder->addDefinition($this->prefix('cachePlugin'))
			->setType(CachePlugin::class)
			->setArguments([
			   $filesystemAdapter,
			   $guzzleStreamFactory,
			   [
				   'cache_lifetime'        => null,
				   'default_ttl'           => null,
				   'respect_cache_headers' => false,
			   ]
			])
			->setAutowired(FALSE);
		$pluginClient = $builder->addDefinition($this->prefix('pluginClient'))
								->setType(PluginClient::class)
								->setArguments([$client, [$cachePlugin]])
								->setAutowired(FALSE);

		$guzzleMessageFactory = $builder->addDefinition($this->prefix('guzzleMessageFactory'))
			->setType(GuzzleMessageFactory::class)
			->setAutowired(FALSE);

		$builder->addDefinition($this->prefix('geocoder'))
			->setType(GeocoderService::class)
			->setArguments([$pluginClient, $guzzleMessageFactory])
			->addSetup('setKey', [$config['apiKey']]);

		$builder->addDefinition($this->prefix('mapBuilderFactory'))
				->setFactory('Ivory\GoogleMap\Helper\Builder\MapHelperBuilder::create')
				->setType(MapHelperBuilder::class)
				->setAutowired(false);

		$builder->addDefinition($this->prefix('mapBuilder'))
				->setFactory('@googleMap.mapBuilderFactory::build')
				->setType(\Ivory\GoogleMap\Helper\MapHelper::class);

		$builder->addDefinition($this->prefix('apiBuilderFactory'))
				->setFactory('\Ivory\GoogleMap\Helper\Builder\ApiHelperBuilder::create')
				->addSetup('setKey', [$config['apiKey']])
				->setType(ApiHelperBuilder::class);

		$builder->addDefinition($this->prefix('apiBuilder'))
			->setFactory( '@googleMap.apiBuilderFactory::build')
			->setType(ApiHelper::class);

		$builder->addDefinition($this->prefix('mapHelper'))
				->setType(MapHelper::class);


	}
}