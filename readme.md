Anticovid19.cz
=============

Aplikace na planovani rozvozu tisku ochranych stitu


Instalace
------------

Aplikaci lze nainstalovat jednoduse tak, ze si zavolate

	composer install

Nasledne vytvorite lokalni config z app/config/local.example.neon


Jeste spustite migrace

	php bin/console m:m

Pro nahrani stylu je potreba spustit npm install a gulp

	npm install && gulp default

Pozadavky
------------

- PHP 7.3
- MySql

Mapa
------------
```php
<?php 

$mapHelper = $container->getByType(MapHelper::class);
$mapBuilder = $container->getByType(\Ivory\GoogleMap\Helper\MapHelper::class);
$apiBuilder = $container->getByType(ApiHelper::class);
$map = $mapHelper->createMap();
$mapHelper->addMarkerToMap('Test', 'Adresa pro geocodovani', $map);
echo $mapBuilder->render($map);
echo $apiBuilder->render([$map]);
```