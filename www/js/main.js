Nette.validators.AppComponentRegisterUserTypeValidator_isDriver = function (elem, arg, value) {
	return jQuery.inArray("8", value) !== -1;
};

$(function(){
	$.nette.init();
	$('.select2').select2({
		theme: 'bootstrap4'
	});

	$('.summernote').summernote()
	$('.select2').on('change', function(e) {
		var form = $(this).closest('form');
		Nette.toggleForm(form[0], this);
	});
	$.nette.ext('snippets').after(function ($el) {
		$el.find('.select2').select2({
			theme: 'bootstrap4'
		});
	});

});

