<?php

declare(strict_types = 1);

namespace App\FrontModule\Presenters;

use App\Component\Address\AddressControl;
use App\Component\Address\IAddressControlFactory;
use App\Entity\Address;
use App\Entity\Product;
use App\Entity\Region;
use App\Grid\GridFactory;
use App\Model\FormFactory;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Zend\Hydrator\DoctrineObject;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Ublaboo\DataGrid\DataGrid;

final class AddressPresenter extends SecuredPresenter
{

	/** @var GridFactory @autowire */
	protected $gridFactory;

	/** @var EntityManager @autowire */
	protected $em;

	/** @var FormFactory @autowire */
	protected $formFactory;

	/** @var DoctrineObject @autowire */
	protected $hydrator;

	/** @var Address|null */
	private $address;

	public function startup()
	{
		parent::startup();
		$currentUser = $this->currentUserAccessor->getCurrentUser();
		if ($currentUser === null || !$currentUser->isAdmin()) {
			throw new ForbiddenRequestException();
		}
	}

	public function actionDetail(int $id  = null)
	{
		$this->address = $id ? $this->em->find(Address::class, $id) : null;
	}

	public function renderDetail()
	{
		$this->template->address = $this->address;
	}
	public function renderDefault(): void
	{

	}

	protected function createComponentGrid(): DataGrid
	{
		$grid = $this->gridFactory->create();
		$grid->setDataSource($this->em->getRepository(Address::class)->createQueryBuilder('a'));
		$grid->addColumnText('name', 'Jméno')->setRenderer(function (Address $address) {
			return Html::el('strong')->setText($address->getName())->addHtml('<br/>')->addHtml(Html::el('small')->setText($address->getCompany()));
		})->setFilterText()->setCondition(function(QueryBuilder $qb, $value) {
			$qb->andWhere('(a.firstName LIKE :name OR a.lastName LIKE :name OR a.company LIKE :name)')->setParameter('name', '%'.$value.'%');
		});
		$grid->addColumnText('center', 'Středisko')->setReplacement([
			true => 'Ano',
			false => 'Ne'
		])->setFilterSelect( ['' => '- vše -', 1 => 'Ano', 0 => 'Ne']);

		$grid->addColumnText('region', 'Region')->setRenderer(function (Address $address) {
			return $address->getRegion() ? $address->getRegion()->getName() : '';
		})->setFilterSelect([null => '- region -'] + $this->em->getRepository(Region::class)->findPairs('name'))->setCondition(function(QueryBuilder $qb, $value) {
			$qb->andWhere('a.region = :region')->setParameter('region', $value);
		});
		$grid->addColumnText('address', 'Adresa')->setRenderer(function ($address) {
			return $address->getLine();
		});
		$grid->addAction('detail', 'Detail');
		return $grid;
	}


	protected function createComponentAddress(IAddressControlFactory $factory): AddressControl
	{
		$control = $factory->create(function() {
			$this->flashMessage('Adresa byla uložena');
			$this->redirect('default');
		});
		if ($this->address) {
			$control->setAddress($this->address);
		}
		return $control;
	}

}
