<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;


use App\Entity\Transport;
use App\Model\CurrentUserAccessor;
use App\Type\TransportStatusType;
use Kdyby\Doctrine\EntityManager;

final class DriverPresenter extends SecuredPresenter
{

	/** @var EntityManager @autowire */
	protected $em;

	/** @var CurrentUserAccessor @autowire */
	protected $currentUserAccessor;

	/** @var Transport|null */
	private $transport;

	public function actionDefault()
	{
		$this->template->myTransports = $this->em->getRepository(Transport::class)->findBy(['driver' => $this->currentUserAccessor->getCurrentUser()]);

		$this->template->availableTransports = $this->em->getRepository(Transport::class)->findBy(['fromAddress.region' => $this->currentUserAccessor->getCurrentUser()->getAddress()->getRegion(), 'driver' => null]);

	}

	public function actionDetail(int $transportId)
	{
		$this->transport = $this->em->find(Transport::class, $transportId);
	}

	public function renderDetail(int $transportId)
	{
		$this->template->transport = $this->transport;
	}

	public function handleSetMeDriver(int $transportId)
	{
		/** @var Transport|null $transport */
		$transport = $this->em->find(Transport::class, $transportId);
		if ($transport->getDriver() !== null) {
			$this->flashMessage('Tento svoz již má nastaveného řidiče.', 'error');
		} else {
			$transport->setDriver($this->currentUserAccessor->getCurrentUser());
			$this->em->flush();
			$this->flashMessage('Byl jste nastaven jako řidič pro tento svoz.', 'success');
		}
		$this->redirect('this');
	}

	public function handleSetStatus(string $status)
	{
		$this->transport->setStatus(TransportStatusType::get($status));
		$this->em->flush();

		$this->redirect('this');
	}
}
