<?php
declare(strict_types = 1);

namespace App\FrontModule\Presenters;

use App\Component\Transport\ITransportControlFactory;
use App\Component\Transport\TransportControl;
use App\Entity\Product;
use App\Entity\Transport;
use App\Grid\GridFactory;
use App\Model\FormFactory;
use App\Type\TransportStatusType;
use Doctrine\Zend\Hydrator\DoctrineObject;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

final class TransportPresenter extends SecuredPresenter
{

	/** @var GridFactory @autowire */
	protected $gridFactory;

	/** @var EntityManager @autowire */
	protected $em;

	/** @var FormFactory @autowire */
	protected $formFactory;

	/** @var DoctrineObject @autowire */
	protected $hydrator;

	/** @var Transport|null */
	private $transport;

	public function startup()
	{
		parent::startup();
		$currentUser = $this->currentUserAccessor->getCurrentUser();
		if ($currentUser === null || !$currentUser->isAdmin()) {
			throw new ForbiddenRequestException();
		}
	}

	public function actionDetail(int $id  = null)
	{
		$this->transport = $id ? $this->em->find(Transport::class, $id) : null;
	}

	public function renderDetail()
	{
		$this->template->transport = $this->transport;
	}
	public function renderDefault(): void
	{

	}

	protected function createComponentGrid()
	{
		$grid = $this->gridFactory->create();
		$grid->setDataSource($this->em->getRepository(Transport::class)->createQueryBuilder('er'));
		$grid->addColumnText('fromAddress', 'Odkud');
		$grid->addColumnText('toAddress', 'Kam');
		$grid->addColumnText('status', 'Stav')->setRenderer(function (Transport $transport) {
			return $transport->getStatus()->getLabel();
		})->setFilterSelect([null => ''] + TransportStatusType::$translations);
		$grid->addColumnText('driver', 'Řidič');
		$grid->addAction('detail', 'Detail');

		return $grid;
	}

	protected function createComponentTransport(ITransportControlFactory $factory): TransportControl
	{
		$control = $factory->create(null, function () {
			$this->flashMessage('Svoz byl vytvořen', 'success');
			$this->redirect('this');
		});
		$control->setTransport($this->transport);
		return $control;
	}
}
