<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

use App\Component\Transport\ITransportControlFactory;
use App\Component\Transport\TransportControl;
use App\Entity\Product;
use App\Entity\Transport;
use App\Entity\UserProduct;
use App\Model\CurrentUserAccessor;
use App\Model\FormFactory;
use App\Type\TransportStatusType;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

final class PrinterPresenter extends SecuredPresenter
{

	/** @var FormFactory @autowire */
	protected $formFactory;

	/** @var EntityManager @autowire */
	protected $em;

	/** @var CurrentUserAccessor @autowire */
	protected $currentUserAccessor;

	/** @var Product[] */
	private $products;

	/** @var Transport|null */
	private $transport;

	protected function createComponentForm(): Form
	{
		$user = $this->currentUserAccessor->getCurrentUser();
		$userProducts = $user->getUserProducts();
		$form = $this->formFactory->create();
		$productsContainer = $form->addContainer('products');
		foreach ($this->products as $product) {
			$productContainer = $productsContainer->addContainer($product->getId());
			$productContainer->addText('quantity', 'Vyrobeno')->addRule(Form::INTEGER);
			$productContainer->addText('quantityProcess', 'Ve výrobě')->addRule(Form::INTEGER);
				}

		if (!$userProducts->isEmpty()) {
			foreach ($userProducts as $userProduct) {
				$form['products'][$userProduct->getProduct()->getId()]['quantity']->setDefaultValue($userProduct->getQuantity());
				$form['products'][$userProduct->getProduct()->getId()]['quantityProcess']->setDefaultValue($userProduct->getQuantityProcess());
			}

		}
		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = function (Form $form, ArrayHash $values) use ($userProducts, $user) {
			foreach ($values->products as $id => $productContainer) {
				$product = $this->em->find(Product::class, $id);

				$userProduct = $userProducts->filter(function (UserProduct $userProduct) use ($product) {
					return $userProduct->getProduct() === $product;
				})->first();
				if (!$userProduct) {
					$userProduct = new UserProduct($user, $product);
					$this->em->persist($userProduct);
				}
				$userProduct->setQuantity($productContainer->quantity);
				$userProduct->setQuantityProcess($productContainer->quantityProcess);
			}
			$this->em->flush();
			$this->flashMessage('Hodnoty byly uloženy', 'success');
			$this->redirect('this');
		};
		return $form;
	}

	public function actionDefault()
	{
		$this->products = $this->em->getRepository(Product::class)->findAll();
		$this->transport = $this->em->getRepository(Transport::class)->findOneBy([
			'fromAddress' => $this->currentUserAccessor->getCurrentUser()->getAddress(),
			'status' => [TransportStatusType::NEW, TransportStatusType::PROCESS]
		]);
	}

	public function renderDefault()
	{
		$this->template->products = $this->products;
		$this->template->transport = $this->transport;
	}

	protected function createComponentTransport(ITransportControlFactory $factory): TransportControl
	{
		return $factory->create($this->currentUserAccessor->getCurrentUser(), function () {
			$this->flashMessage('Svoz byl vytvořen', 'success');
			$this->redirect('this');
		});
	}
}
