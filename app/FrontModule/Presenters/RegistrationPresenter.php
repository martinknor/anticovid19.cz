<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;


use App\Component\Register\IRegisterControlFactory;
use App\Component\Register\RegisterControl;

final class RegistrationPresenter extends BasePresenter
{

	/** @var IRegisterControlFactory */
	protected $registerControlFactory;

	public function __construct(IRegisterControlFactory $registerControlFactory)
	{
		parent::__construct();
		$this->registerControlFactory = $registerControlFactory;
	}

	public function renderDefault(): void
	{

	}

	protected function createComponentRegistrationControl(): RegisterControl
	{
		$control = $this->registerControlFactory->create(function() {
			$this->redirect('success');
		});

		return $control;
	}
}
