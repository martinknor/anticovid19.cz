<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

use App\Entity\User;
use App\Model\UserManager;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Nette\Security\AuthenticationException;
use Throwable;

final class LoginPresenter extends BasePresenter
{

	/** @var UserManager */
	protected $userManager;

	public function __construct(UserManager $userManager)
	{
		parent::__construct();
		$this->userManager = $userManager;
	}

	public function actionDefault(): void
	{
		if ($this->getUser()->isLoggedIn()) {
			$this->redirect('Homepage:default');
		}
	}


	public function actionLogout(): void
	{
		$this->getUser()->logout();
		$this->flashMessage('Byl/a jste úspěšně odhlášen/a');
		$this->redirect('default');
	}


	public function actionResetPassword(): void
	{

	}

	public function createComponentChangePassword(): Form
	{
		$form = new Form;
		$form->addPassword('password', 'Nové heslo')->addRule(Form::MIN_LENGTH, 'Heslo musí mít minimálně %d znaků', 6)->setRequired();
		$form->addSubmit('send', 'Změnit');

		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues(true);
			$this->userManager->changePassword($this->passwordRequest->getUser(), $values['password']);
			$this->passwordRequest->setChanged(true);
			$this->em->flush();
			$this->flashMessage('Heslo bylo úspěšně změněno. Můžete se přihlásit.', 'success');
			$this->redirect('default');
		};
		return $form;
	}

	/**
	 * @param string $hash
	 * @throws AbortException
	 */
	public function actionNewPassword(string $hash): void
	{
		/** @var PasswordResetRequest $request */
		$request = $this->em->getRepository(PasswordResetRequest::class)->findOneBy(['hash' => $hash]);
		if (!$request || $request->isExpired() || $request->isChanged()) {
			$this->flashMessage('Tato žádost o změnu hesla je neplatná. Vygenerujte si novou.');
			$this->redirect('resetPassword');
		}

		$this->template->passwordRequest = $this->passwordRequest = $request;
	}

	public function createComponentResetPasswordForm(): Form
	{
		$form = new Form();
		$form->addText('login', 'Login')->setRequired();
		$form->addSubmit('send', 'Resetovat heslo');
		$form->onSuccess[] = [$this, 'reset'];

		return $form;
	}


	public function createComponentLoginForm(): Form
	{
		$form = new Form();
		$form->addText('login', 'Login')->setRequired();
		$form->addPassword('password', 'Heslo')->setRequired();
		$form->addSubmit('send', 'Přihlásit se');
		$form->onSuccess[] = [$this, 'login'];

		return $form;
	}

	public function reset($form, $values): void
	{
		/** @var User $user */
		$user = $this->em->getRepository(User::class)->createQueryBuilder('u')->where('u.login = :login AND (u.deletedAt > NOW() OR u.deletedAt IS NULL)')->setParameter('login', $values->login)->getQuery()->getOneOrNullResult();

		if (!$user) {
			$this->flashMessage('Uživatel nebyl nalezen', 'danger');
		} else {
			try {
				$this->userManager->createPasswordResetRequest($user, function (PasswordResetRequest $passwordResetRequest) {
					$this->resetPasswordMail->send($passwordResetRequest);
				});
			} catch (Throwable $e) {
				$this->flashMessage('Chyba při vytváření požadavku na změnu hesla.', 'danger');
				$this->redirect('this');
			}
			$this->flashMessage('Informace pro změnu hesla Vám byly odeslány na mail.', 'success');
			$this->redirect('default');
		}
	}

	public function login($form, $values): void
	{
		try {
			$this->user->setExpiration('+ 1 days');
			$user = $this->user->login($values->login, $values->password);
			$this->redirect('Homepage:default');

		} catch (AuthenticationException $e) {
			$this->flashMessage('Nepodařilo se Vám přihlásit', 'danger');
			$this->redirect('this');
		}
	}
}
