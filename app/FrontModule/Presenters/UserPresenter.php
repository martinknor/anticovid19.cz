<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;


use App\Entity\Product;
use App\Entity\Region;
use App\Entity\User;
use App\Grid\GridFactory;
use App\Type\UserType;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\QueryBuilder;
use Nette\Application\ForbiddenRequestException;
use Nette\Utils\Html;

final class UserPresenter extends SecuredPresenter
{

	/** @var GridFactory @autowire */
	protected $gridFactory;

	/** @var EntityManager @autowire */
	protected $em;

	/** @var User|null */
	private $userEntity;

	public function startup()
	{
		parent::startup();
		$currentUser = $this->currentUserAccessor->getCurrentUser();
		if ($currentUser === null || !$currentUser->isAdmin()) {
			throw new ForbiddenRequestException();
		}
	}
	public function actionDetail(int $id  = null)
	{
		$this->userEntity = $id ? $this->em->find(User::class, $id) : null;
	}

	public function renderDetail()
	{
		$this->template->userEntity = $this->userEntity;
	}

	public function renderDefault(): void
	{

	}


	protected function createComponentGrid()
	{
		$grid = $this->gridFactory->create();
		$grid->setDataSource($this->em->getRepository(User::class)->createQueryBuilder('u'));
		$grid->addColumnText('name', 'Jméno')->setRenderer(function (User $user) {
			return Html::el('strong')->setText($user->getAddress()->getName())->addHtml('<br/>')->addHtml(Html::el('small')->setText($user->getLogin()));
		})->setFilterText()->setCondition(function(QueryBuilder $qb, $value) {
			$qb->join('u.address', 'a')->andWhere('(a.firstName LIKE :name OR a.lastName LIKE :name)')->setParameter('name', '%'.$value.'%');
		});
		$grid->addColumnText('typ', 'Typ')->setRenderer(function (User $user) {
			$val = Html::el();
			foreach ($user->getType()->getLabels() as $color => $name) {
				$val->addHtml(Html::el('span', ['class' => 'badge badge-light mx-1', 'style' => 'background-color: ' . $color])->setText($name));
			}
			return $val;
		})->setFilterSelect([null => '- typ -'] + UserType::$translations)->setCondition(function(QueryBuilder $qb, $value) {
			$qb->andWhere('BIT_AND(u.type, :type) = :type')->setParameter('type', $value);
		});
		
		$grid->addColumnText('region', 'Region')->setRenderer(function (User $user) {
			return $user->getAddress()->getRegion() ? $user->getAddress()->getRegion()->getName() : '';
		})->setFilterSelect([null => '- region -'] + $this->em->getRepository(Region::class)->findPairs('name'))->setCondition(function(QueryBuilder $qb, $value) {
			$qb->join('u.address', 'a')->andWhere('a.region = :region')->setParameter('region', $value);
		});
		/*$grid->addColumnText('address', 'Adresa')->setRenderer(function (User $user) {
			return $user->getAddress()->getLine();
		});*/
		$grid->addAction('detail', 'Detail');
		return $grid;
	}
}
