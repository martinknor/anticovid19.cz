<?php declare(strict_types = 1);

namespace App\FrontModule\Presenters;

use App\Model\CurrentUserAccessor;

class SecuredPresenter extends BasePresenter
{

	/** @var CurrentUserAccessor @autowire */
	protected $currentUserAccessor;

	public function __construct(CurrentUserAccessor $currentUserAccessor)
	{
		parent::__construct();
		$this->currentUserAccessor = $currentUserAccessor;
	}

	public function startup()
	{
		parent::startup();

		if (!$this->currentUserAccessor->getCurrentUser()) {
			$this->redirect('Login:', ['backLink' => $this->storeRequest()]);
		}
	}
}
