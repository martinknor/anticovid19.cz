<?php

declare(strict_types=1);

namespace App\FrontModule\Presenters;

use App\Model\CurrentUserAccessor;
use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	use AutowireProperties;
	use AutowireComponentFactories;

	/** @var CurrentUserAccessor @autowire */
	protected $currentUserAccessor;

	protected function beforeRender()
	{
		parent::beforeRender();
		$this->template->currentUser = $this->currentUserAccessor->getCurrentUser();
	}
}
