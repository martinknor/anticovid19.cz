<?php

declare(strict_types = 1);

namespace App\FrontModule\Presenters;

use App\Entity\Product;
use App\Grid\GridFactory;
use App\Model\FormFactory;
use Doctrine\Zend\Hydrator\DoctrineObject;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\ForbiddenRequestException;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

final class ProductPresenter extends SecuredPresenter
{

	/** @var GridFactory @autowire */
	protected $gridFactory;

	/** @var EntityManager @autowire */
	protected $em;

	/** @var FormFactory @autowire */
	protected $formFactory;

	/** @var DoctrineObject @autowire */
	protected $hydrator;

	/** @var Product|null */
	private $product;

	public function startup()
	{
		parent::startup();
		$currentUser = $this->currentUserAccessor->getCurrentUser();
		if ($currentUser === null || !$currentUser->isAdmin()) {
			throw new ForbiddenRequestException();
		}
	}

	public function actionDetail(int $id  = null)
	{
		$this->product = $id ? $this->em->find(Product::class, $id) : null;
	}

	public function renderDetail()
	{
		$this->template->product = $this->product;
	}
	public function renderDefault(): void
	{

	}

	protected function createComponentGrid()
	{
		$grid = $this->gridFactory->create();
		$grid->setDataSource($this->em->getRepository(Product::class)->createQueryBuilder('er'));
		$grid->addColumnText('code', 'Kód');
		$grid->addColumnText('name', 'Název');
		$grid->addAction('detail', 'Detail');

		return $grid;
	}

	protected function createComponentProduct()
	{
		$form = $this->formFactory->create();
		$form->addText('code', 'Kód');
		$form->addText('name', 'Název');
		$form->addTextArea('description', 'Popis')->getControlPrototype()->addAttributes(['class' => 'summernote']);
		$form->addSubmit('save', 'Uložit');
		if ($this->product) {
			$form->setDefaults($this->hydrator->extract($this->product));
		}
		$form->onSuccess[] = function (Form $form, ArrayHash $values) {
			if ($this->product === null) {
				$this->product = new Product($values->code, $values->name);
				$this->em->persist($this->product);
			}
			$this->hydrator->hydrate((array) $values, $this->product);
			$this->em->flush();
			$this->flashMessage('Produkt byl uložen', 'success');
			$this->redirect('default');
		};
		return $form;
	}
}
