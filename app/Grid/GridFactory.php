<?php
declare(strict_types = 1);

namespace App\Grid;

use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Localization\SimpleTranslator;

class GridFactory
{

	public function create(): DataGrid
	{
		$grid = new DataGrid();
		$this->setup($grid);

		return $grid;
	}

	public function setup(DataGrid $grid)
	{

		$grid->setItemsPerPageList([10, 20, 50, 100, 200], false);
		$grid->setDefaultPerPage(20);
		$translator = new SimpleTranslator([
			'ublaboo_datagrid.no_item_found_reset' => 'Žádné položky nenalezeny. Filtr můžete vynulovat',
			'ublaboo_datagrid.no_item_found' => 'Žádné položky nenalezeny.',
			'ublaboo_datagrid.here' => 'zde',
			'ublaboo_datagrid.items' => 'Položky',
			'ublaboo_datagrid.all' => 'všechny',
			'ublaboo_datagrid.from' => 'z',
			'ublaboo_datagrid.reset_filter' => 'Resetovat filtr',
			'ublaboo_datagrid.group_actions' => 'Hromadné akce',
			'ublaboo_datagrid.show_all_columns' => 'Zobrazit všechny sloupce',
			'ublaboo_datagrid.hide_column' => 'Skrýt sloupec',
			'ublaboo_datagrid.action' => 'Akce',
			'ublaboo_datagrid.previous' => 'Předchozí',
			'ublaboo_datagrid.next' => 'Další',
			'ublaboo_datagrid.choose' => 'Vyberte',
			'ublaboo_datagrid.execute' => 'Provést',
			'ublaboo_datagrid.per_page_submit' => 'Změnit',
			'Name' => 'Jméno',
			'Inserted' => 'Vloženo'

		]);

		$grid->setTranslator($translator);
	}
}
