<?php
declare(strict_types = 1);

namespace App\Subscriber;

use Consistence\Doctrine\Enum\EnumPostLoadEntityListener;
use Doctrine\ORM\Events;
use Kdyby\Events\Subscriber;

class EnumPostLoadEntitySubscriber extends EnumPostLoadEntityListener implements Subscriber
{

	public function getSubscribedEvents()
	{
		return [ Events::postLoad];
	}
}
