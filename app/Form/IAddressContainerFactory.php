<?php
declare(strict_types = 1);

namespace App\Form;

interface IAddressContainerFactory
{

	public function create(): AddressContainer;
}
