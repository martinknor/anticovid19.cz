<?php
declare(strict_types = 1);

namespace App\Form;

use App\Entity\Address;
use App\Entity\Country;
use App\Entity\Region;
use Doctrine\Zend\Hydrator\DoctrineObject;
use Kdyby\Doctrine\EntityManager;
use Nette\Forms\Container;
use Nette\Forms\Form;
use function array_key_first;

class AddressContainer extends Container
{

	/** @var DoctrineObject */
	protected $hydrator;

	/** @var EntityManager */
	protected $em;

	public function __construct(DoctrineObject $hydrator, EntityManager $em)
	{
		$this->hydrator = $hydrator;
		$this->em = $em;

		$this->addText('firstName', 'Jméno')->setRequired();
		$this->addText('lastName', 'Příjmení')->setRequired();
		$this->addText('company', 'Firma');
		$this->addText('additional', 'Upřesnění adresy');
		$this->addText('street', 'Ulice')->setRequired();
		$this->addText('houseNo', 'Číslo')->addRule(Form::INTEGER)->setRequired();
		$this->addText('city', 'Město')->setRequired();
		$this->addText('postcode', 'PSČ')->setRequired();
		$this->addText('phone', 'Telefon')->setRequired();
		$emailControl = $this->addText('email', 'Email')->addCondition(Form::FILLED)->addRule(Form::EMAIL);

		$this->addSelect('country', 'Země', $countries = $this->em->getRepository(Country::class)->findPairs('name'))->setDefaultValue(array_key_first($countries))->setRequired();

		$this->addSelect('region', 'Kraj', $this->em->getRepository(Region::class)->findPairs('name'))->setRequired()->setPrompt('vyberte kraj');

		$this->addCheckbox('center', 'Středisko');
	}

	public function setAddress(Address $address = null)
	{
		$data = $address ? $this->hydrator->extract($address) : [];
		if (isset($data['country'])) {
			$data['country'] = $data['country'] ? $data['country']->getId() : null;
		}
		if (isset($data['region'])) {
			$data['region'] = $data['region'] ? $data['region']->getId() : null;
		}
		$this->setDefaults($data);
	}

	public function createOrUpdateAddress(Address $address = null): Address
	{
		$address = $address ?? new Address();
		$this->em->persist($address);
		$this->hydrator->hydrate((array) $this->getValues(), $address);

		return $address;

	}
}