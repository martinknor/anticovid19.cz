<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Routing\Route;

final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;
		$router->addRoute('admin[/<presenter>][/<action>]', [
			'module' => 'Admin',
			'presenter' => 'Homepage',
			'action' => 'default'
		]);
		$router->addRoute('<presenter>/<action>', [
			'module' => 'Front',
			'presenter' => [
				Route::VALUE => 'Homepage',
				Route::FILTER_TABLE => [
					// řetězec v URL => presenter
					'registrace' => 'Registration',
					'prihlaseni' => 'Login',
					'ridic' => 'Driver',
					'tiskarna' => 'Printer',
					'clanek' => 'Page',
					'uzivatele' => 'User',
					'produkty' => 'Product',
					'objednavka' => 'Order',
					'adresar' => 'Address',
				],
			],
			'action' => 'default'
		]);
		return $router;
	}
}
