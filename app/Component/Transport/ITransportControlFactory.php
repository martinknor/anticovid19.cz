<?php
declare(strict_types = 1);

namespace App\Component\Transport;

use App\Entity\User;

interface ITransportControlFactory
{

	public function create(User $user = null, callable $onSuccess): TransportControl;
}
