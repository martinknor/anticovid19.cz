<?php
declare(strict_types = 1);

namespace App\Component\Transport;

use App\Entity\Address;
use App\Entity\Transport;
use App\Entity\User;
use App\Form\IAddressContainerFactory;
use App\Model\FormFactory;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Zend\Hydrator\DoctrineObject;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;
use function call_user_func;

class TransportControl extends Control
{

	/** @var FormFactory */
	protected $formFactory;

	/** @var callable */
	protected $onSuccess;

	/** @var EntityManager */
	protected $em;

	/** @var DoctrineObject */
	protected $hydrator;

	/** @var User */
	protected $user;

	/** @var Transport|null */
	private $transport;

	public function __construct(User $user = null, callable $onSuccess, DoctrineObject $hydrator, FormFactory $formFactory, EntityManager $em)
	{
		$this->formFactory = $formFactory;
		$this->onSuccess = $onSuccess;
		$this->em = $em;
		$this->hydrator = $hydrator;
		$this->user = $user;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/template.latte');
		$this->template->render();
	}

	public function setTransport(Transport $transport = null)
	{
		$this->transport = $transport;
	}

	protected function createComponentForm(): Form
	{
		$form = $this->formFactory->create();
		if ($this->user === null) {
			$address = new ArrayCollection($this->em->getRepository(Address::class)->findAssoc([], 'id'));
			$fromAddress = $address->map(function (Address $address) {
				return $address->getLine();
			})->toArray();
			$form->addSelect('fromAddress', 'Odkud', $fromAddress)->setPrompt('- vyberte -')->setRequired();
		}
		$adress = new ArrayCollection($this->em->getRepository(Address::class)->findAssoc(['center' => true], 'id'));
		$toAddress = $adress;
		if ($this->user) {
			$region = $this->user->getAddress()->getRegion();
			$toAddress = $adress->filter(function (Address $address) use ($region) {
				return $address->getRegion() === $region;
			});
			if ($toAddress->isEmpty()) {
				$toAddress = $adress;
			}
		}
		$toAddress = $toAddress->map(function (Address $address) {
			return $address->getLine();
		})->toArray();
		$form->addSelect('toAddress', 'Kam', $toAddress)->setPrompt('- vyberte -')->setRequired(); #vybrat strediska (nejblizsi?)
		$form->addTextArea('description', 'Popis');
		if ($this->transport) {
			$form->setDefaults([
				'fromAddress' => $this->transport->getFromAddress()->getId(),
				'toAddress' => $this->transport->getToAddress()->getId(),
				'description' => $this->transport->getDescription(),
			]);
		}
		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = function (Form $form, ArrayHash $values) {
			$this->em->beginTransaction();
			$fromAddress = $this->user ? $this->user->getAddress() : $this->em->find(Address::class, $values->fromAddress);
			if ($this->transport === null) {
				$this->transport = new Transport($fromAddress);
				$this->transport->setCreatedUser($this->user);
				$this->em->persist($this->transport);
			}
			$this->hydrator->hydrate((array) $values, $this->transport);
			$this->em->flush();
			$this->em->commit();

			call_user_func($this->onSuccess);
		};

		$form->onError[] = function () {

		};

		return $form;
	}
}