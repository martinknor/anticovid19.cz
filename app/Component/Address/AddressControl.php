<?php
declare(strict_types = 1);

namespace App\Component\Address;

use App\Entity\Address;
use App\Form\IAddressContainerFactory;
use App\Model\FormFactory;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;
use function call_user_func;

class AddressControl extends Control
{

	/** @var FormFactory */
	protected $formFactory;

	/** @var callable */
	protected $onSuccess;

	/** @var IAddressContainerFactory */
	protected $addressContainerFactory;

	/** @var EntityManager */
	protected $em;

	/** @var Address|null */
	protected $address;

	public function __construct(callable $onSuccess, FormFactory $formFactory, EntityManager $em, IAddressContainerFactory $addressContainerFactory)
	{
		$this->formFactory = $formFactory;
		$this->onSuccess = $onSuccess;
		$this->addressContainerFactory = $addressContainerFactory;
		$this->em = $em;
	}

	public function setAddress(?Address $address): void
	{
		$this->address = $address;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/template.latte');
		$this->template->render();
	}

	protected function createComponentForm()
	{
		$form = $this->formFactory->create();
		$addressContainer = $this->addressContainerFactory->create();
		$addressContainer->setAddress($this->address);
		$form->addComponent($addressContainer, 'address');
		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = function (Form $form, ArrayHash $values) use ($addressContainer) {
			$this->em->beginTransaction();
			$addressContainer->createOrUpdateAddress($this->address);
			$this->em->flush();
			$this->em->commit();

			call_user_func($this->onSuccess);
		};

		$form->onError[] = function () {

		};

		return $form;
	}
}