<?php
declare(strict_types = 1);

namespace App\Component\Address;

interface IAddressControlFactory
{

	public function create(callable $onSuccess): AddressControl;
}
