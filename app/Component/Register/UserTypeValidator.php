<?php
declare(strict_types = 1);

namespace App\Component\Register;

use App\Type\UserType;
use Nette\Forms\IControl;
use function in_array;

class UserTypeValidator
{
	public CONST DRIVER = 'App\Component\Register\UserTypeValidator::isDriver';

	public static function isDriver(IControl $control): bool
	{
		return in_array(UserType::DRIVER, $control->getValue(), true);
	}
}