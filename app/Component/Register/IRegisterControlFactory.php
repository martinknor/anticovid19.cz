<?php
declare(strict_types = 1);

namespace App\Component\Register;

interface IRegisterControlFactory
{

	public function create(callable $onSuccess): RegisterControl;
}
