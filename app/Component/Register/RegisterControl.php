<?php
declare(strict_types = 1);

namespace App\Component\Register;

use App\Entity\Address;
use App\Entity\Country;
use App\Entity\Driver;
use App\Entity\Region;
use App\Form\IAddressContainerFactory;
use App\Model\FormFactory;
use App\Model\UserManager;
use App\Type\UserType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Zend\Hydrator\DoctrineObject;
use Kdyby\Doctrine\EntityManager;
use Nette\Application\UI\Control;
use Nette\Forms\Container;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;
use function array_key_first;
use function call_user_func;

class RegisterControl extends Control
{
	/** @var FormFactory */
	protected $formFactory;

	/** @var DoctrineObject */
	protected $hydrator;

	/** @var UserManager */
	protected $userManager;

	/** @var EntityManager */
	protected $em;

	/** @var callable */
	protected $onSuccess;

	/** @var IAddressContainerFactory */
	protected $addressContainerFactory;

	public function __construct(callable $onSuccess, FormFactory $formFactory, DoctrineObject $hydrator, UserManager $userManager, EntityManager $em, IAddressContainerFactory $addressContainerFactory)
	{
		$this->formFactory = $formFactory;
		$this->hydrator = $hydrator;
		$this->userManager = $userManager;
		$this->em = $em;
		$this->onSuccess = $onSuccess;
		$this->addressContainerFactory = $addressContainerFactory;
	}

	protected function createComponentForm()
	{
		$form = $this->formFactory->create();

		$types = UserType::$translations;
		unset($types[UserType::ADMIN]);
		unset($types[UserType::CENTER]);
		$form->addMultiSelect('type', 'Vyberte typ uživatele', $types)->setRequired()->addCondition(UserTypeValidator::DRIVER)->toggle('driver');
		$form->addText('login', 'Váš email pro přihlášení')->addRule(Form::EMAIL)->setRequired();
		$passwordControl = $form->addPassword('password', 'Heslo');
		$passwordControl->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 6)->setRequired();
		$form->addPassword('passwordVerify', 'Heslo znovu')->addRule(Form::EQUAL, 'Hesla musí být stejná', $passwordControl)->setRequired();
		$addressContainer = $this->addressContainerFactory->create();
		$form->addComponent($addressContainer, 'address');


		#driver
		$driverContainer = new Container();
		$driverContainer->addSelect('radius', 'Mohu doručovat v rozmezí', [
			5 => '+- 5 km',
			10 => '+- 10 km',
			25 => '+- 25 km',
			50 => '+- 50 km',
			100 => '+- 100 km',
			150 => '+- 150 km',
			1000 => 'celá ČR',
		]);
		
		$form->addComponent($driverContainer, 'driver');
		$form->addCheckbox('terms', 'Souhlas')->setRequired();
		$form->addSubmit('save', 'Registrovat');
		$form->onSuccess[] = function (Form $form, ArrayHash $values) use ($addressContainer) {
			$this->em->beginTransaction();
			try {

				$user = $this->userManager->add($values->login, $values->password, UserType::getMulti(...$values->type));

				$address = $addressContainer->createOrUpdateAddress();
				$address->setEmail($values->login);
				$user->setAddress($address);

				if ($user->isDriver()) {
					$driver = new Driver($user, $values->driver->radius);
					$this->em->persist($driver);
				}
				$this->em->flush();
				$this->em->commit();
			} catch (UniqueConstraintViolationException $e) {
				$this->em->rollback();
				$form->addError('Uživatel již existuje.');
				return;
			}
			call_user_func($this->onSuccess);
		};

		$form->onError[] = function () {

		};
		#printer

		return $form;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/template.latte');
		$this->template->render();
	}
}