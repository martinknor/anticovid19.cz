<?php
declare(strict_types = 1);

namespace App\Model;

use Nette\Application\UI\Form;
use Nextras\FormsRendering\Renderers\Bs4FormRenderer;
use Nextras\FormsRendering\Renderers\FormLayout;

class FormFactory
{

	public function create($layout = FormLayout::HORIZONTAL): Form
	{
		$form = new Form();
		$form->setRenderer(new Bs4FormRenderer($layout));

		return $form;
	}
}