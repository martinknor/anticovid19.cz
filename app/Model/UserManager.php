<?php

declare(strict_types=1);

namespace App\Model;

use App\Entity\User;
use App\Type\UserType;
use Kdyby\Doctrine\EntityManager;
use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
final class UserManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;

	/** @var EntityManager */
	protected $em;

	/** @var Passwords */
	private $passwords;


	public function __construct(EntityManager $em, Passwords $passwords)
	{
		$this->passwords = $passwords;
		$this->em = $em;
	}


	/**
	 * Performs an authentication.
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials): Nette\Security\IIdentity
	{
		[$username, $password] = $credentials;

		/** @var User|null $user */
		$user = $this->em->getRepository(User::class)->findOneBy(['login' => $username]);

		if (!$user) {
			throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);

		} elseif (!$this->passwords->verify($password, $user->getPasswordHash())) {
			throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

		} elseif ($this->passwords->needsRehash($user->getPasswordHash())) {
			$user->setPasswordHash($this->passwords->hash($password));
			$this->em->flush($user);
		}

		return new Nette\Security\Identity($user->getId());
	}


	public function add(string $username, string $password, UserType $userType): User
	{
		Nette\Utils\Validators::assert($username, 'email');
		$user = new User($username, $this->passwords->hash($password), $userType);

		$this->em->persist($user);
		$this->em->flush();
		return $user;
	}
}
