<?php
declare(strict_types = 1);

namespace App\Model;

use App\Entity\User as UserEntity;
use Kdyby\Doctrine\EntityManager;
use Nette\Security\User;

class CurrentUserAccessor
{

	/** @var EntityManager */
	protected $em;

	/** @var User */
	private $user;

	public function __construct(User $user, EntityManager $em)
	{
		$this->user = $user;
		$this->em = $em;
	}

	public function getCurrentUser(): ?UserEntity
	{
		if ($this->user->isLoggedIn()) {
			return $this->em->find(UserEntity::class, $this->user->getId());
		}

		return null;
	}
}