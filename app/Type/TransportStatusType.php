<?php
declare(strict_types = 1);

namespace App\Type;

use Consistence\Enum\Enum;
use Nette\Utils\Html;

class TransportStatusType extends Enum
{

	public CONST NEW = 'new';

	public CONST PROCESS = 'process';

	public CONST DONE = 'done';

	public CONST CANCELED = 'canceled';

	public static $translations = [
		self::NEW      => 'nový',
		self::PROCESS  => 'vyřizovaný',
		self::DONE     => 'dokončený',
		self::CANCELED => 'zrušený',

	];

	public static $color = [
		self::NEW      => '#CCCCCC',
		self::PROCESS  => '#70D8FA',
		self::DONE     => '#85FF89',
		self::CANCELED => '#FFA385',
	];

	public function __toString()
	{
		return (string) self::$translations[$this->getValue()];
	}

	public function getLabel()
	{
		$color = self::$color[$this->getValue()];
		return Html::el('span', ['class' => 'badge', 'style' => 'background-color: ' . $color])->setText(self::$translations[$this->getValue()]);
	}
}