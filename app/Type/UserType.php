<?php
declare(strict_types = 1);

namespace App\Type;

use Consistence\Enum\MultiEnum;

class UserType extends MultiEnum
{

	public CONST ADMIN = 1;

	public CONST PRINTER = 2;

	public CONST CUSTOMER = 4;

	public CONST DRIVER = 8;

	public CONST CENTER = 16;

	public CONST PROGRAMMER = 32;

	public static $translations = [
		self::ADMIN    => 'admin',
		self::PRINTER  => 'mám 3D tiskárnu',
		self::CUSTOMER => 'potřebuji ochranu',
		self::DRIVER   => 'jsem řidič',
		self::CENTER   => 'středisko',
		self::PROGRAMMER   => 'programátor :)',
	];

	public static $color = [
		self::ADMIN    => 'red',
		self::PRINTER  => 'blue',
		self::CUSTOMER => 'green',
		self::DRIVER   => 'orange',
		self::CENTER   => 'brown',
		self::PROGRAMMER   => 'puprle',
	];

	/**
	 * @return array of color => name
	 */
	public function getLabels(): array
	{
		$labels = [];
		foreach ($this->getValues() as $value) {
			$labels[self::$color[$value]] = self::$translations[$value];
		}
		return $labels;
	}

}