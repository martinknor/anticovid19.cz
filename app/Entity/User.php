<?php
declare(strict_types = 1);

namespace App\Entity;

use App\Type\UserType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Consistence\Doctrine\Enum\EnumAnnotation as Enum;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 */
class User
{

	use Identifier;

	/**
	 * @ORM\OneToMany(targetEntity="UserProduct", mappedBy="user")
	 * @var UserProduct[]
	 */
	protected $userProducts;

	/**
	 * @Enum(class=UserType::class)
	 * @ORM\Column(type="integer_enum", nullable=false)
	 * @var UserType
	 */
	private $type;

	/**
	 * @ORM\Column(type="string", unique=true)
	 * @var string
	 */
	private $login;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	private $passwordHash;

	/**
	 * @ORM\OneToOne(targetEntity="Address")
	 * @var Address|null
	 */
	private $address;

	/**
	 * @ORM\OneToOne(targetEntity="Driver", mappedBy="user")
	 * @var Driver|null
	 */
	private $driver;

	/**
	 * @ORM\OneToOne(targetEntity="Printer", mappedBy="user")
	 * @var Printer|null
	 */
	private $printer;

	public function __construct(string $login, string $passwordHash, UserType $type)
	{
		$this->userProducts = new ArrayCollection();
		$this->login = $login;
		$this->passwordHash = $passwordHash;
		$this->setType($type);
	}

	public function getLogin(): string
	{
		return $this->login;
	}

	public function setLogin(string $login): void
	{
		$this->login = $login;
	}

	public function getPasswordHash(): string
	{
		return $this->passwordHash;
	}

	public function setPasswordHash(string $passwordHash): void
	{
		$this->passwordHash = $passwordHash;
	}

	public function getType(): UserType
	{
		return $this->type;
	}

	public function setType(UserType $type): void
	{
		$this->type = $type;
	}

	public function getAddress(): ?Address
	{
		return $this->address;
	}

	public function setAddress(?Address $address): void
	{
		$this->address = $address;
	}

	public function isDriver(): bool
	{
		return $this->getType()->containsValue(UserType::DRIVER);
	}

	public function isPrinter(): bool
	{
		return $this->getType()->containsValue(UserType::PRINTER);
	}

	public function isCustomer(): bool
	{
		return $this->getType()->containsValue(UserType::CUSTOMER);
	}

	public function isAdmin(): bool
	{
		return $this->getType()->containsValue(UserType::ADMIN);
	}

	public function getName(): ?string
	{
		return $this->getAddress() ? $this->getAddress()->getName() : null;
	}

	/**
	 * @return ArrayCollection|UserProduct[]
	 */
	public function getUserProducts(): Collection
	{
		return $this->userProducts;
	}

	public function setUserProducts(array $userProducts): void
	{
		$this->userProducts = $userProducts;
	}

	public function __toString()
	{
		return $this->getAddress()->getName();
	}
}