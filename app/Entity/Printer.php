<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 */
class Printer
{

	use Identifier;

	/**
	 * @ORM\OneToOne(targetEntity="User")
	 * @var User
	 */
	private $user;



	public function __construct(User $user)
	{
		$this->user = $user;
	}
}