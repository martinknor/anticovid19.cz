<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 */
class UserProduct
{
	use Identifier;

	/**
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="userProducts")
	 * @var User
	 */
	protected $user;

	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="userProducts")
	 * @var Product
	 */
	protected $product;

	/**
	 * Vyrobeno, pocet doma
	 * @ORM\Column(type="integer", options={"default":0})
	 * @var int
	 */
	protected $quantity = 0;

	/**
	 * Ve vyrobe
	 * @ORM\Column(type="integer", options={"default":0})
	 * @var int
	 */
	protected $quantityProcess = 0;

	/**
	 * Zmetci
	 * @ORM\Column(type="integer", options={"default":0})
	 * @var int
	 */
	protected $quantityBad = 0;

	/**
	 * Vyzvednuto
	 * @ORM\Column(type="integer", options={"default":0})
	 * @var int
	 */
	protected $quantityDone = 0;

	public function __construct(User $user, Product $product)
	{
		$this->user = $user;
		$this->product = $product;
	}

	public function getUser(): User
	{
		return $this->user;
	}

	public function setUser(User $user): void
	{
		$this->user = $user;
	}

	public function getProduct(): Product
	{
		return $this->product;
	}

	public function setProduct(Product $product): void
	{
		$this->product = $product;
	}

	public function getQuantity(): int
	{
		return $this->quantity;
	}

	public function setQuantity(int $quantity): void
	{
		$this->quantity = $quantity;
	}

	public function getQuantityProcess(): int
	{
		return $this->quantityProcess;
	}

	public function setQuantityProcess(int $quantityProcess): void
	{
		$this->quantityProcess = $quantityProcess;
	}

	public function getQuantityBad(): int
	{
		return $this->quantityBad;
	}

	public function setQuantityBad(int $quantityBad): void
	{
		$this->quantityBad = $quantityBad;
	}

	public function getQuantityDone(): int
	{
		return $this->quantityDone;
	}

	public function setQuantityDone(int $quantityDone): void
	{
		$this->quantityDone = $quantityDone;
	}

}