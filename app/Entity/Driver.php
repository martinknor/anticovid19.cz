<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 */
class Driver
{

	use Identifier;

	/**
	 * @ORM\OneToOne(targetEntity="User", inversedBy="driver")
	 * @var User
	 */
	private $user;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	protected $radius;

	public function __construct(User $user, int $radius)
	{
		$this->user = $user;
		$this->radius = $radius;
	}
}