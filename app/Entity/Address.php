<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 */
class Address
{

	use Identifier;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $firstName;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $lastName;

	/**
	 * @ORM\Column(type="boolean", options={"default":0})
	 * @var bool
	 */
	protected $center = false;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $company;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $additional;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $street;

	/**
	 * @var string|null
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $houseNo;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $city;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	protected $postcode;

	/**
	 * @ORM\ManyToOne(targetEntity="Country")
	 * @var Country|null
	 */
	protected $country;

	/**
	 * @ORM\ManyToOne(targetEntity="Region")
	 * @var Region|null
	 */
	protected $region;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string}null
	 */
	private $email;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string|null
	 */
	private $phone;

	public function getCompany(): ?string
	{
		return $this->company;
	}

	public function setCompany(?string $company): void
	{
		$this->company = $company;
	}

	public function getAdditional(): ?string
	{
		return $this->additional;
	}

	public function setAdditional(string $additional): void
	{
		$this->additional = $additional;
	}

	public function getLine(): string
	{
		return $this->getName() . ', ' . $this->getStreetWithHouseNo() . ', ' . $this->getCity() . ' ' . $this->getPostcode() . ', ' . $this->getCountry()->getName() . ($this->getRegion() ? (' '
																																															   . $this->getRegion()
																																																	  ->getName())
				: '');
	}

	public function getName(): string
	{
		return $this->getFirstName() . ($this->getFirstName() && $this->getLastName() ? ' ' : null) . $this->getLastName();
	}

	public function getStreetWithHouseNo(): string
	{
		return $this->street . ' ' . $this->houseNo;
	}

	public function getCity(): string
	{
		return $this->city;
	}

	public function setCity(string $city): void
	{
		$this->city = $city;
	}

	public function getPostcode(): string
	{
		return $this->postcode;
	}

	public function setPostcode(string $postcode): void
	{
		$this->postcode = $postcode;
	}

	public function getCountry(): Country
	{
		return $this->country;
	}

	public function setCountry(Country $country): void
	{
		$this->country = $country;
	}

	public function getRegion(): ?Region
	{
		return $this->region;
	}

	public function getFirstName(): ?string
	{
		return $this->firstName;
	}

	public function setFirstName(?string $firstName): void
	{
		$this->firstName = $firstName;
	}

	public function getLastName(): ?string
	{
		return $this->lastName;
	}

	public function setLastName(?string $lastName): void
	{
		$this->lastName = $lastName;
	}

	public function getStreet(): string
	{
		return $this->street;
	}

	public function setStreet(string $street): void
	{
		$this->street = $street;
	}

	public function getHouseNo(): string
	{
		return $this->houseNo;
	}

	public function setHouseNo(string $houseNo)
	{
		$this->houseNo = $houseNo;
	}

	public function getEmail(): ?string
	{
		return $this->email;
	}

	public function setEmail(string $email = null): void
	{
		$this->email = $email;
	}

	public function getPhone(): ?string
	{
		return $this->phone;
	}

	public function setPhone(?string $phone): void
	{
		$this->phone = $phone;
	}

	public function setRegion(?Region $region): void
	{
		$this->region = $region;
	}

	public function isCenter(): bool
	{
		return $this->center;
	}

	public function setCenter(bool $center): void
	{
		$this->center = $center;
	}

	public function __toString()
	{
		return $this->getLine();
	}
}
