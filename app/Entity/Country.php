<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 */
class Country
{

	use Identifier;

	/**
	 * Contry code in ISO 3166-1 alpha-2
	 * @see https://en.wikipedia.org/wiki/ISO_3166-1#Current_codes
	 * @ORM\Column(type="string", length=2)
	 * @var string
	 */
	protected $code;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $name;

	public function getCode(): string
	{
		return $this->code;
	}

	public function setCode(string $code): void
	{
		$this->code = $code;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}
}
