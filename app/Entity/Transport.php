<?php
declare(strict_types = 1);

namespace App\Entity;

use App\Type\TransportStatusType;
use App\Type\UserType;
use Consistence\Doctrine\Enum\EnumAnnotation as Enum;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 */
class Transport
{

	use Identifier;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $description;

	/**
	 *
	 * @ORM\ManyToOne(targetEntity="User")
	 * @ORM\JoinColumn(nullable=false)
	 * @var User
	 */
	protected $createdUser;

	/**
	 * @ORM\ManyToOne(targetEntity="User")
	 * @var User|null
	 */
	protected $driver;

	/**
	 * @ORM\ManyToOne(targetEntity="Address")
	 * @ORM\JoinColumn(nullable=false)
	 * @var Address
	 */
	private $fromAddress;

	/**
	 * @ORM\ManyToOne(targetEntity="Address")
	 * @var Address|null
	 */
	private $toAddress;

	/**
	 * @Enum(class=TransportStatusType::class)
	 * @ORM\Column(type="string_enum", nullable=false)
	 * @var TransportStatusType
	 */
	private $status;

	public function __construct(?Address $fromAddress)
	{
		$this->status = TransportStatusType::get(TransportStatusType::NEW);
		$this->fromAddress = $fromAddress;
	}

	public function getFromAddress(): ?Address
	{
		return $this->fromAddress;
	}

	public function setFromAddress(?Address $fromAddress): void
	{
		$this->fromAddress = $fromAddress;
	}

	public function getToAddress(): ?Address
	{
		return $this->toAddress;
	}

	public function setToAddress(?Address $toAddress): void
	{
		$this->toAddress = $toAddress;
	}

	public function getDescription(): string
	{
		return $this->description;
	}

	public function setDescription(string $description): void
	{
		$this->description = $description;
	}

	public function getDriver(): ?User
	{
		return $this->driver;
	}

	public function setDriver(?User $driver): void
	{
		$this->driver = $driver;
		$this->setStatus(TransportStatusType::get(TransportStatusType::PROCESS));
	}

	public function getStatus(): TransportStatusType
	{
		return $this->status;
	}

	public function setStatus(TransportStatusType $status): void
	{
		$this->status = $status;
	}

	public function getCreatedUser(): User
	{
		return $this->createdUser;
	}

	public function setCreatedUser(User $createdUser): void
	{
		$this->createdUser = $createdUser;
	}
}