<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 */
class Product
{

	use Identifier;

	/**
	 * @ORM\OneToMany(targetEntity="UserProduct", mappedBy="product")
	 * @var UserProduct
	 */
	protected $userProducts;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $code;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	protected $name;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	protected $description;

	public function __construct(string $code, string $name)
	{

		$this->name = $name;
		$this->code = $code;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	public function getCode(): string
	{
		return $this->code;
	}

	public function setCode(string $code): void
	{
		$this->code = $code;
	}

	public function getDescription(): string
	{
		return $this->description;
	}

	public function setDescription(string $description): void
	{
		$this->description = $description;
	}
}